=== Extend VdoCipher ===
Contributors:      tradesouthwestgmailcom
Donate link:       https://paypal.me/tradesouthwest/
Tags:              custom
Plugin URI:        https://themes.tradesouthwest.com/plugins
Requires at least: 4.6
Tested up to:      5.3.1
Requires PHP:      5.4
Stable tag:        1.0.2
Text Domain:       extend-vdocipher
Domain Path:       /languages
License:           GPL v2 or later
License URI:       https://www.gnu.org/licenses/gpl-2.0.txt

Customise functions for VdoCipher plugin.

== Change Log ==
1.0.2
* latest release

== Notes ==
Clock from https://www.webnots.com/alt-code-shortcuts-for-time-related-symbols/ 
@see https://www.vdocipher.com/docs/player/v2/api-reference/accessing-player/
