<?php
/**
 * Prevent direct access to the file.
 * @subpackage extend-vdocipher/inc/extend-vdocipher-admin.php
 * @since 1.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Add colum for extvdo_viewed_totals and register the column
 * 
 * @since 1.0.2
 * @return $columns
 */
add_filter( 'manage_users_columns', 'extend_vdocipher_user_viewed_column' );
function extend_vdocipher_user_viewed_column( $columns ) {
    $limits = ( empty( get_option('extend_vdocipher_options')['extend_vdocipher_timelimit'])) 
                 ? 0 : get_option('extend_vdocipher_options')['extend_vdocipher_timelimit'];
       
	$columns[ 'extvdo_viewed_totals' ] = esc_attr($limits);
	return $columns;
}

/**
 * populate the column with data
 * 
 * @since 1.0.2
 * @return $columns
 */
add_action( 'manage_users_custom_column',  'extend_vdocipher_column_content', 10, 3);
function extend_vdocipher_column_content( $value, $column_name, $user_id ) {
	if( 'extvdo_viewed_totals' === $column_name ) {
     
        $seconds = get_user_meta( $user_id, 'extvdo_viewed_totals', true );
		return $seconds;
	}
	return $value;
}

/**
 * Add CSS for the column
 * 
 * @since 1.0.2
 * @return $columns
 */
add_action( 'admin_head-users.php',  'extend_vdocipher_column_style' );
function extend_vdocipher_column_style(){
	echo '<style>.column-extvdo_viewed_totals{ width: 5%;font-size:10px!important }</style>';
}

// A3
add_action('extend_vdocipher_sendmail_test','extend_vdocipher_sendmail_onexpired', 10, 2); 
// F1
//remove_filter('wp_mail_from_name', 'extend_vdocipher_wp_mail_from_name');
/** 
 * F1
 * Send email filter [Not used]
 * @uses TSW_Sendmail class
 * 
 * @since 1.0.2
 */
function extend_vdocipher_wp_mail_from_name($name) {
    $name = get_option('sitename');
    return $name;
}
/**
 * A3
 * Send test or real email if viewed has expired
 * 
 * @since 1.0.2
 */

function extend_vdocipher_sendmail_onexpired( $user_name, $user_email )
{
    $sending = false;
    $to      = sanitize_email( $user_email );
    $subject = 'Mawatynki Training Videos Membership Notification';
    $body    = $user_name .',<h4>Your Mawatynki viewtime of videos has expired. Please renew you subscription.</h4>
                <p>Renew to access all training videos. To renew your account please visit: </p>
                <p><a href="https://mawatynki.co.uk/register/your-membership/" title="renew mawatynki membership link">Login to Mawatynki</a></p>
                <p>Or copy/paste this link into your browser: https://mawatynki.co.uk/register/your-membership/</p>
                <p><img src="https://mawatynki.co.uk/wp-content/uploads/2023/10/mawatynkipngalt.png" height="100" alt="Mawatynki the carer\'s companion"/></p>';
    $headers = array('Content-Type: text/html; charset=UTF-8');

    //$to, $subject, $message, $headers, $attachments 
    $sent    = wp_mail( $to, $subject, $body, $headers );
    
    if ( $sent ) {
        $sending = true;
    }
    if( $sending ){
        echo 'sent: &nbsp; ' . $to . ' ' . $subject . '<br>' . $body;
    } else {
        echo 'NOT sent';
    }  
} 

/**
 * Automatically add note to admin Notes of Members Membership editor page.
 * @param #rcp-addmembership-not name="new_note" 
 */
function extend_vdocipher_rcp_textarea_notes()
{
    return false;
} 

/**
 * Extend VdoCipher Options Page
 *
 * Add options page for the plugin.
 *
 * @since 1.0
 */
function extend_vdocipher_options_plugin_page() {

	add_options_page(
		__( 'Extend VdoCipher Options', 'extend-vdocipher' ),
		__( 'Extend VdoCipher', 'extend-vdocipher' ),
		'manage_options',
		'extend-vdocipher',
		'extend_vdocipher_render_admin_page'
	);

}
add_action( 'admin_menu', 'extend_vdocipher_options_plugin_page' );
add_action( 'admin_init', 'extend_vdocipher_register_admin_options' ); 
/**
 * Register settings for options page
 *
 * @since    1.0.0
 * 
 * a.) register all settings groups
 * Register Settings $option_group, $option_name, $sanitize_callback 
 */
function extend_vdocipher_register_admin_options() 
{
    
    register_setting( 'extend_vdocipher_options', 'extend_vdocipher_options' );
        
    //add a section to admin page
    register_setting( 'extend_vdocipher_options', 'extend_vdocipher_options' );
        
    //add a section to admin page
    add_settings_section(
        'extend_vdocipher_options_settings_section',
        '',
        'extend_vdocipher_options_settings_section_callback',
        'extend_vdocipher_options'
    );
    add_settings_field(
        'extend_vdocipher_print_styles',
        __( 'Label Editor', 'extend-vdocipher' ),
        'extend_vdocipher_print_styles_cb',
        'extend_vdocipher_options',
        'extend_vdocipher_options_settings_section',
        array( 
            'type'        => 'text',
            'option_name' => 'extend_vdocipher_options', 
            'name'        => 'extend_vdocipher_print_styles',
            'value'       => ( empty( get_option('extend_vdocipher_options')['extend_vdocipher_print_styles'] )) 
                            ? false : get_option('extend_vdocipher_options')['extend_vdocipher_print_styles'],
            'default'     => '',
            'description' => esc_html__( 'Enter text for timer field', 'extend_vdocipher' )
        ) 
    );    
    add_settings_field(
        'extend_vdocipher_timelimit',
        __( 'Time Limits', 'extend-vdocipher' ),
        'extend_vdocipher_timelimit_cb',
        'extend_vdocipher_options',
        'extend_vdocipher_options_settings_section',
        array( 
            'type'        => 'number',
            'option_name' => 'extend_vdocipher_options', 
            'name'        => 'extend_vdocipher_timelimit',
            'value'       => ( empty( get_option('extend_vdocipher_options')['extend_vdocipher_timelimit'] )) 
                            ? 0 : get_option('extend_vdocipher_options')['extend_vdocipher_timelimit'],
            'default'     => '',
            'description' => esc_html__( 'How many seconds can viewers use per membership?', 'extend_vdocipher' )
        ) 
    );    
    // settings checkbox 
    add_settings_field(
        'extend_vdocipher_styles_radio',
        __('Activate the Pullout', 'extend-vdocipher'),
        'extend_vdocipher_styles_radio_cb',
        'extend_vdocipher_options',
        'extend_vdocipher_options_settings_section',
        array( 
            'type'        => 'checkbox',
            'option_name' => 'extend_vdocipher_options', 
            'name'        => 'extend_vdocipher_styles_radio',
            'value'       => ( !isset( get_option( 'extend_vdocipher_options')['extend_vdocipher_styles_radio']))
                ? 0 : get_option('extend_vdocipher_options')['extend_vdocipher_styles_radio'],
            'checked'     => ( get_option('extend_vdocipher_options')['extend_vdocipher_styles_radio'] == 0) 
                                ? '' : 'checked',
            'description' => esc_html__( 'Check to use timer. Uncheck to disable.', 'extend-vdocipher' ),
            'tip'         => esc_attr__( 'Default is OFF. Check to continue using pullout.', 'extend-vdocipher' )  
        )
    ); 
    // settings checkbox 
    add_settings_field(
        'extend_vdocipher_rcpemail',
        __('Activate Expiry eMail Notices', 'extend-vdocipher'),
        'extend_vdocipher_rcpemail_cb',
        'extend_vdocipher_options',
        'extend_vdocipher_options_settings_section',
        array( 
            'type'        => 'checkbox',
            'option_name' => 'extend_vdocipher_options', 
            'name'        => 'extend_vdocipher_rcpemail',
            'value'       => ( !isset( get_option( 'extend_vdocipher_options')['extend_vdocipher_rcpemail']))
                ? 0 : get_option('extend_vdocipher_options')['extend_vdocipher_styles_radio'],
            'checked'     => ( get_option('extend_vdocipher_options')['extend_vdocipher_rcpemail'] == 0) 
                                ? '' : 'checked',
            'description' => esc_html__( 'Check to allow sending expired time emails. Uncheck to disable.', 'extend-vdocipher' ),
            'tip'         => esc_attr__( 'Default is OFF. Check to continue sending.', 'extend-vdocipher' )  
        )
    ); 
}

/** 
 * render for 'text' field
 * @since 1.0.0
 */
function extend_vdocipher_print_styles_cb($args)
{  
    printf(
    '<fieldset>
    <p><span class="vmarg">%4$s </span></p>
    <input id="%1$s" class="text-field" name="%2$s[%1$s]" value="%3$s" size=95>     
    </fieldset>',
        $args['name'],
        $args['option_name'],
        $args['value'],
        $args['description']
    );
}
/** 
 * render for 'number' field
 * @since 1.0.0
 */
function extend_vdocipher_timelimit_cb($args)
{  
    printf(
    '<fieldset>
    <p><span class="vmarg">%4$s </span></p>
    <p><input id="%1$s" class="text-field" name="%2$s[%1$s]" value="%3$s" size=15><br>
    <span>%6$s</span></p>
    <small>%5$s </small> 
    </fieldset>',
        $args['name'],
        $args['option_name'],
        $args['value'],
        $args['description'],
        '1 hr = 3,600 s; 40 hr = 144,000 s; https://www.inchcalculator.com/convert/hour-to-second/',
        extend_vdocipher_convert_timer( $args['value'] )
    );
}
/** 
 * render for '0' field
 * @since 1.0.0
 */
function extend_vdocipher_styles_radio_cb($args)
{  
    printf(
        '<fieldset>
        <input type="hidden" name="%3$s[%1$s]" value="0">
        <input id="%1$s" type="%2$s" name="%3$s[%1$s]" value="1"  
        class="regular-checkbox" %7$s /> 
        <sup class="grctip" title="%6$s"><strong>?</strong></sup><br>
        <span class="vmarg">%5$s </span> 
        </fieldset>',
            $args['name'],
            $args['type'],
            $args['option_name'],
            $args['value'],
            $args['description'],
            $args['tip'],
            $args['checked']
        );
}
/** 
 * render for '0' field
 * @since 1.0.0
 */
function extend_vdocipher_rcpemail_cb($args)
{  
    printf(
        '<fieldset>
        <input type="hidden" name="%3$s[%1$s]" value="0">
        <input id="%1$s" type="%2$s" name="%3$s[%1$s]" value="1"  
        class="regular-checkbox" %7$s /> 
        <sup class="grctip" title="%6$s"><strong>?</strong></sup><br>
        <span class="vmarg">%5$s </span>
        <p>Remember to <a href="%8$s" target="_blank" title="opens in new window">Update Memberships manually</a></p>
        </fieldset>',
            $args['name'],
            $args['type'],
            $args['option_name'],
            $args['value'],
            $args['description'],
            $args['tip'],
            $args['checked'],
            esc_url('https://mawatynki.co.uk/wp-admin/admin.php?page=rcp-members')
        );
}
//callback for description of options section
function extend_vdocipher_options_settings_section_callback() 
{
	echo '<h2>' . esc_html__( 'Extend Vdocipher Settings', 'extend-vdocipher' ) . '</h2>';
}
// display the plugin settings page
function extend_vdocipher_render_admin_page()
{
	// check if user is allowed access
    if ( ! current_user_can( 'manage_options' ) ) return;
    
	echo '<form action="options.php" method="post">';

	// output security fields
	settings_fields( 'extend_vdocipher_options' );

	// output setting sections
	do_settings_sections( 'extend_vdocipher_options' );
	submit_button();

    echo '</form>'; 

    echo '<h4>Manual eMail of Expired Notice </h4>
    <form id="extvdo-expire-email" method="POST" action="">
        <p><label>email</label><input id="extvdo_expire_email" type="text" 
            name="extvdo_expire_email" 
            value="" size=35></p>
        <p><label>name</label><input id="extvdo_expire_user" type="text" 
            name="extvdo_expire_user" 
            value="" size=35></p>

        <p><input type="submit" name="extvdo_testmail" value="send-now"></p>'
        . wp_nonce_field( 'extvdo-email-test' ) . '
        </form>
        <small>You do not have to click the Save Changes ever, when sending email. 
        JUST click "send now"</small>'; 
        
        if (isset($_POST['extvdo_testmail'])&& $_POST['extvdo_testmail'] 
            == 'send-now') {

        $user_name = isset($_POST['extvdo_expire_user']) 
              ? wp_unslash($_POST['extvdo_expire_user']) : '';
          
        $user_email = isset($_POST['extvdo_expire_email']) 
               ? wp_unslash($_POST['extvdo_expire_email']) : '';

        do_action( 'extend_vdocipher_sendmail_test', $user_name, $user_email ); 
        } 
} 
