<?php
/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin Shortcodes.
 *
 * @link       https://tradesouthwest.com
 * @since      1.0.0
 *
 * @package    Extend_Vdocipher
 * @subpackage shortcodes
 */
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Extend vdo shortcode for testing only
 * 
 * @param string $currvid  The ID of the option being displayed.
 * @uses shortcode         [extvdo_tags]
 * @since 1.0.0
 */
add_shortcode('extvdo_tags', 'extend_vdocipher_shortcode_tags');

function extend_vdocipher_shortcode_tags(){
    
    $currvid = extend_vdocipher_get_current_video();
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => 
    "https://www.vdocipher.com/api/videos/$currvid/?keys=length",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "chunked",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Content-Type: application/json",
        "Authorization: Apisecret xxxxx"
        ),
    ));

    $response = curl_exec($curl);
    $err      = curl_error($curl);
    curl_close($curl);

    if ($err) {
    echo "cURL Error #:" . $err;
    } else {
    echo $response;
    } 

}

/**
 * Extend vdo shortcode for testing only
 * 
 * @param string $currvid  The ID of the option being displayed.
 * 
 * @since 1.0.0
 */
function extend_vdocipher_get_current_video(){

    $currvid = '9a4265be047744d2b3f10819f3a3b603';

    return $currvid;
} 


/**
 * Extend vdo shortcode for template rcp/subscription
 * 
 * @param string $  The ID of the option being displayed.
 * @uses shortcode         [extend-vdocipher-account]
 * @since 1.0.0
 */
add_shortcode('extend-vdocipher-account', 'extend_vdocipher_shortcode_account');

function extend_vdocipher_shortcode_account()
{
    $user_id = $first_name = $last_name = $extvdo_timed = $extvdo_text = '';
    $warns = false;
    if ( is_user_logged_in() ){
        $user_id     = get_current_user_id();
        $first_name  = get_user_meta( $user_id, 'first_name', true );
        $last_name   = get_user_meta( $user_id, 'last_name', true );
        $extvdo_time = get_user_meta( $user_id, 'extvdo_viewed_totals', true );
        $extvdo_text = ( '' != get_option('extend_vdocipher_options')['extend_vdocipher_print_styles'] )
                    ? get_option('extend_vdocipher_options')['extend_vdocipher_print_styles']
                    : 'Total time: ';
        $extvdo_limt = ( '' != get_option('extend_vdocipher_options')['extend_vdocipher_timelimit'] )
                    ? get_option('extend_vdocipher_options')['extend_vdocipher_timelimit']
                    : '';

        if ( $extvdo_time > $extvdo_limt ) $warns = true;
    }
    if ( $warns == true ) { 
        $notice = __( 'View Time Expired - Please Renew Now', 'extend-vdocipher' ); 
    } else {
        $notice = '';
    }

    ob_start(); ?>
    <table class="rcp-table time-viewed" id="rcp-account-timeviewed">
    <thead class="hide-mobile">
        <tr>
            <th><?php _e( $extvdo_text, 'extend-vdocipher' ); ?></th>
            <th><?php echo esc_html( $first_name . ' ' . $last_name ); ?></th>
            <th><?php esc_html_e( 'Limit', 'extend-vdocipher' ); ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo esc_html( extend_vdocipher_convert_timer( 
                $extvdo_time ) ); ?></td>
            <td><p class="warning"><?php echo esc_html( $notice ); ?></p></td>
            <td><?php echo esc_html( extend_vdocipher_convert_timer( 
                $extvdo_limt ) ); ?></td>
        </tr>
    </tbody>
    </table>
    <?php 
    $htmlout = ob_get_clean();

        echo $htmlout;
} 
