<?php
// exit if file is called directly
if ( ! defined( 'ABSPATH' ) ) {	exit; }
// A1
add_action( 'extend_vdocipher_ifexpired', 'extend_vdocipher_check_ifexpired' );
// A2
add_action( 'extend_vdocipher_sendmail_renew',
           'extend_vdocipher_sendmail_isexpired', 10, 2 ); 

/**
 * Retrieves information about the user who is currently logged into the site.
 *
 * This function is intended to be called via the client-side. Not server.
 * @param AJAX Endpoint 
 * @since 1.0.2
 */
function extvdo_prepend_timer()
{
    $chkif     = false;
    $user_id   = get_current_user_id();
    // Retrieves user meta field for a user.
    $seconds   = get_user_meta( $user_id, 'extvdo_viewed_totals', true );
    // from ajax post
    $viewedNew = sanitize_text_field( $_POST['viewedNew'] );
    $new_total = absint( $seconds + $viewedNew );
    
    if ( $new_total > 0 ) {
        $extvdo_total = update_user_meta( 
            $user_id, 
            'extvdo_viewed_totals', 
            sanitize_text_field( $new_total ) 
        );  
        /*
        $chkif =  do_action( 'extend_vdocipher_ifexpired', $user_id );
        
        if ( $chkif === true ) :
                $user_name  = get_user_meta( $user_id, 'user_name' );
                $user_email = get_user_meta( $user_id, 'user_email' );
                do_action('extend_vdocipher_sendmail_test', 
                            $user_name, 
                            $user_email 
                        );
        endif;
        */
        // Sends a JSON response back to an Ajax request, indicating success.
        wp_send_json_success( $new_total );
    }
 
    die(); 
}

/** A1
 * Get user data and compare with extvdo option
 *
 * @uses action extend_vdocipher_ifexpired
 * @since 1.0.0
 * @return Boolean
 */

 function extend_vdocipher_check_ifexpired( $user_id)
 {
     $rtn     = false;
     $seconds = get_user_meta( $user_id, 'extvdo_viewed_totals', true );
 
     if ( $seconds > $limits ) $rtn = true;
 
         return $rtn;
 
}

/** A2
 * Get user data and compare with extvdo option
 *
 * @since 1.0.0
 * @return WP_Mail()
 */

function extend_vdocipher_sendmail_isexpired( $user_name, $user_email )
{
    $sending = false;
    $to      = sanitize_email( $user_email );
    $subject = 'Mawatynki Training Videos Membership Notification';
    $body    = $user_name .',<h4>Your Mawatynki viewtime of videos has expired. Please renew you subscription.</h4>
                <p>Renew to access all training videos. To renew your account please visit: </p>
                <p><a href="https://mawatynki.co.uk/register/your-membership/" title="renew mawatynki membership link">Login to Mawatynki</a></p>
                <p>Or copy/paste this link into your browser: https://mawatynki.co.uk/register/your-membership/</p>
                <p><img src="https://mawatynki.co.uk/wp-content/uploads/2023/10/mawatynkipngalt.png" height="100" alt="Mawatynki the carer\'s companion"/></p>';
    $headers = array('Content-Type: text/html; charset=UTF-8');

    //$to, $subject, $message, $headers, $attachments 
    $sent    = wp_mail( $to, $subject, $body, $headers );
    
    if ( $sent ) {
        $sending = true;
    }
    if( $sending ){
        echo 'Email sent'; //&nbsp; ' . $to . ' ' . $subject . '<br>' . $body;
    } else {
        echo 'NOT sent';
    }  
} 

/**
 * Start timer and add to usermeta
 * @since 1.0.1
 * @return string $time_viewed
 */
function extend_vdocipher_get_time_on_page()
{
    $user_id      = get_current_user_id();
    $extvdo_timed = get_user_meta( $user_id, 'extvdo_viewed_totals', true );
    
        return ( '' != $extvdo_timed ) ? $extvdo_timed : '0';
}

/**
 * Add timer to page
 * @since 1.0.2
 * @return $content HTML content
 */
add_filter('the_content','extend_vdocipher_content_prepend_timer');

function extend_vdocipher_content_prepend_timer($content)
{
    global $post; 
    // check if page is a child page
    if ( is_page() && $post->post_parent ) { 
    //$user_id      = get_current_user_id();
    //$extvdo_time  = get_user_meta( $user_id, 'extvdo_viewed_totals', true );
    $extvdo_timed = extend_vdocipher_get_time_on_page();
    $extvdo_check = ( '' != get_option('extend_vdocipher_options')['extend_vdocipher_styles_radio'] )
                    ? get_option('extend_vdocipher_options')['extend_vdocipher_styles_radio']
                    : 1;
    $extvdo_text  = ( '' != get_option('extend_vdocipher_options')['extend_vdocipher_print_styles'] )
                    ? get_option('extend_vdocipher_options')['extend_vdocipher_print_styles']
                    : '';
    $extvdo_link  = "https://mawatynki.co.uk/register/your-membership/";
    $extvdo_check = ( 0 != $extvdo_check ) ? 'block' : 'none'; 
    
    ob_start();
    
    echo
    '<section class="extvdo-pullout-wrapper" style="display:'. $extvdo_check .'">
        <div id="extvdoTimed" class="extvdo-timed">

            <div class="extvdo-total-played extvdo-tp" id="tp"></div>
            <div class="extvdo-total-played extvdo-tc" id="tc"></div>

        <form id="extvdoForm" method="POST">
            <p class="extvdo-input"><small>' . esc_html__( $extvdo_text ) . '</small>
            <input id="extvdo-prepend-convert" 
            class="extvdo-field" 
            type="text" 
            name="extvdo_prepend_timer" 
            value="'. esc_attr(extend_vdocipher_convert_timer($extvdo_timed)) .'">
            <input id="extvdo-prepend-timer" 
            class="extvdo-timer-field" 
            type="hidden" 
            name="extvdo_prepend_timer" 
            value="' . esc_attr( $extvdo_timed ) . '">
            </p>
        </form>
            <p><a class="extvdo-link-button" href="' . esc_url( $extvdo_link ) . '" 
                title="my account (opens in new window/tab)" target="_blank">My Account</a></p>
            <span class="extvdo-puller"><span>Check</span><button id="extvdoUpdata">🕓</button>Time</span>
            
        </div>
            
    </section>';

    $contentz = ob_get_clean();
    
        return $content . $contentz;

    } else {
        return $content;
    }
}

//PHP program to convert seconds into
//hours, minutes, and seconds
function extend_vdocipher_convert_timer($seconds)
{
    $seconds = ( '' != $seconds ) ? $seconds : 0;
    $secs = absint( $seconds % 60 );
    $hrs  = absint( $seconds / 60 );
    $mins = $hrs % 60;
    $hrs  = $hrs / 60;

    return ( (int)$hrs . "Hr " . (int)$mins . "m " . (int)$secs . "s" );
}

/**
 * Custom user profile fields.
 *
 * @param $user
 * @author Larry Judd
 */

function extend_vdocipher_custom_user_profile_fields( $user ) {

    $ut = get_the_author_meta( 'extvdo_viewed_totals', $user->ID );
    echo '<h3 class="heading">'. esc_html__('Video Views', '') .'</h3>';
    ?>
    <table class="form-table"><tbody>
        <tr>
            <th><label for="extvdo_viewed_totals" title="total time viewed">
                <?php esc_html_e( 'Total time viewed', 'extend-vdocipher' ); ?></label></th>
            <td>
                <input type="text" name="extvdo_viewed_totals" id="extvdo_viewed_totals" 
                value="<?php echo esc_attr( $ut ); ?>" 
                class="regular-text" />
                <?php echo esc_attr( extend_vdocipher_convert_timer($ut) ); ?>
            </td>
        </tr>
    </tbody></table>
    <?php
}
add_action( 'show_user_profile', 'extend_vdocipher_custom_user_profile_fields' );
add_action( 'edit_user_profile', 'extend_vdocipher_custom_user_profile_fields' );

/**
 * Save custom user profile fields.
 * Only admins can change these values.
 * @param User Id $user_id
 */
function extend_vdocipher_save_custom_user_profile_fields( $user_id ) {
    if ( current_user_can( 'manage_options', $user_id ) ) {

        update_user_meta( $user_id, 'extvdo_viewed_totals', 
            sanitize_text_field( $_POST['extvdo_viewed_totals'] ) );

    }
}
add_action( 'personal_options_update', 'extend_vdocipher_save_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'extend_vdocipher_save_custom_user_profile_fields' );

/** 
 * Not used
 * @since 1.0.0
 * @uses vdo_otp($video, $otp_post_array = array());
 */
function extend_vdocipher_annotate_display($vdo_annotate_code)
{
    
    $fullname = "";
    if (is_user_logged_in()) {
        $current_user = wp_get_current_user();
        $firstname    = $current_user->first_name;
        $lastname     = $current_user->last_name;
        $fullname     = $firstname . " " . $lastname;
    }
    if ( '' != $fullname ) {
        $vdo_annotate_code = str_replace(
                                '{fullname}', 
                                $fullname, 
                                $vdo_annotate_code 
                            );
    }
        return $vdo_annotate_code;
}
//add_filter('vdocipher_annotate_preprocess', 'extend_vdocipher_annotate_display'); 

/**
 * Get option table option name.
 *
 * @since 1.0.0
 */
function extend_vdocipher_find_option($opt)
{
    $opt     = ( '' != $opt ) ? $opt : '';
    $options = get_option('extend_vdocipher_option');

    $option  = $options["extend_vdocipher_option_{$opt}"];
    $rtrn    = ( '' != $option ) ? $option : '';

        return $rtrn;
        $opt = null;
} 
