<?php
/**
 * Plugin Name:       Extend VdoCipher
 * Plugin URI:        http://themes.tradesouthwest.com/wordpress/plugins/
 * Description:       Extended functionality for VdoCipher. Opens in Settings > ExtVdo Options
 * Author:            tradesouthwestgmailcom
 * Author URI:        https://tradesouthwest.com
 * Version:           1.0.2
 * License:           GPLv2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-3.0.html
 * Requires at least: 4.5
 * Tested up to:      6.1.1
 * Requires PHP:      5.4
 * Text Domain:       extend-vdocipher
 * Domain Path:       /languages
*/

// exit if file is called directly
if ( ! defined( 'ABSPATH' ) ) {	exit; }
/** 
 * Constants
 * 
 * @param EXTEND_VDOCIPHER_VER         Using bumped ver.
 * @param EXTEND_VDOCIPHER_URL         Base path
 * @since 1.0.0 
 */
if( !defined( 'EXTEND_VDOCIPHER_VER' )) { 
      define( 'EXTEND_VDOCIPHER_VER', '1.0.2' ); }
if( !defined( 'EXTEND_VDOCIPHER_URL' )) { 
      define( 'EXTEND_VDOCIPHER_URL', plugin_dir_url(__FILE__)); }

    // Start the plugin when it is loaded.
    register_activation_hook(   __FILE__, 
                        'extend_vdocipher_plugin_activation' );
    register_deactivation_hook( __FILE__, 
                        'extend_vdocipher_plugin_deactivation' );
/**
 * Activate/deactivate hooks
 * 
 */
function extend_vdocipher_plugin_activation() 
{

    return false;
}
function extend_vdocipher_plugin_deactivation() 
{
    return false;
} 
/**
 * Plugin Scripts
 *
 * Register and Enqueues plugin scripts
 *
 * @since 1.0.0
 */
function extend_vdocipher_addtosite_scripts()
{

    wp_enqueue_style( 'extend-vdocipher-public',  
        plugin_dir_url(__FILE__) . 'static/extend-vdocipher-public.css',
        array(), 
        EXTEND_VDOCIPHER_VER, 
        false 
    );

    wp_enqueue_script( 'extend-vdocipher-timetrack', 
        plugin_dir_url( __FILE__ ) . 'static/timetrack-extvdo.js', 
        array( ), 
        EXTEND_VDOCIPHER_VER, 
        true
    ); 

    wp_localize_script( 'extvdo-prepend-timer-action',
        'extvdo_prepend_timer',
        array( 
        'adminAjax' => admin_url( 'admin-ajax.php' ),
        'security' => wp_create_nonce( 'extvdo-prepend-timer-action-nonce' )
        ));
    
    wp_enqueue_script( 'extvdo-prepend-timer', 
        plugin_dir_url( __FILE__ ) . 'static/extend-vdocipher-front.js', 
        array( 'jquery' ), 
        EXTEND_VDOCIPHER_VER, 
        true 
    ); 
}
add_action( 'wp_enqueue_scripts', 'extend_vdocipher_addtosite_scripts' );
add_action( 'wp_ajax_extvdo_prepend_timer', 'extvdo_prepend_timer');
add_action( 'wp_ajax_nopriv_extvdo_prepend_timer', 'extvdo_prepend_timer');

/**
 * Define the locale for this plugin for internationalization.
 * Set the domain and register the hook with WordPress.
 *
 * @uses slug `extend-vdocipher`
 */
add_action( 'plugins_loaded', 'extend_vdocipher_load_plugin_textdomain' );

function extend_vdocipher_load_plugin_textdomain() 
{

    $plugin_dir = basename( dirname(__FILE__) ) .'/languages';
                  load_plugin_textdomain( 'extend-vdocipher', false, $plugin_dir );
}  

require_once ( plugin_dir_path(__FILE__) . 'inc/extend-vdocipher-functions.php' );
require_once ( plugin_dir_path(__FILE__) . 'inc/extend-vdocipher-admin.php' );
require_once ( plugin_dir_path(__FILE__) . 'inc/extend-vdocipher-shortcodes.php' );
