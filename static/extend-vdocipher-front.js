/* scripts for extend vdocipher @see https://www.vdocipher.com/docs/player/v2/examples/inject-html/ 
https://www.vdocipher.com/docs/player/v2/examples/time-track/ */

(function( $ ) {
	'use strict';

$(document).ready(function() {
    
    //const player = VdoPlayer.getInstance(iframe);

    let extvdoCnt = localStorage.getItem('extvdoPrependTimer');
    console.log('locStrg: ' + extvdoCnt);

        //extvdoTimed
        $('#extvdoUpdata').on('click', function() {
            var viewedNew = $('#tp').text();
            $.ajax({
                url:  '/wp-admin/admin-ajax.php',
			    type: 'post',
			    data:{
				action: 'extvdo_prepend_timer',
				viewedNew: viewedNew,
			    },
                success: function() {
                    console.log( viewedNew + ' OK' ); 
                },
                error: function(err) {
                    console.error( err + ' No' + viewedNew );
                }
            })
        });
    }); 
})( jQuery ); 
