/* extend-vdocipher iframe count https://www.vdocipher.com/docs/player/v2/api-reference/accessing-player/ */
document.addEventListener("DOMContentLoaded",  () => {
  //console.log("Page is Ready");
  
  const tpEl = document.querySelector("#tp");
  const tcEl = document.querySelector("#tc");
  const iframe = document.querySelector("iframe");
  // get instance of Vdo
  const player = VdoPlayer.getInstance(iframe);

  player.video.addEventListener("play", function () {
    //console.log("Video is playing");
    // val of input
    let extvdo_prepend_timer = document.getElementById('extvdo-prepend-timer').value;
    localStorage.setItem('extvdoPrependTimer', extvdo_prepend_timer);

    setInterval(() => {
      player.api.getTotalPlayed().then((tp) => {
        tpEl.innerHTML = tp;
      });
      player.api.getTotalCovered().then((tc) => {
        tcEl.innerHTML = `Total covered: ${tc}`;
      });
    }, 1000);
  });
  player.video.addEventListener("pause", function () {
    //console.log(e); 
    // timer aka #tp
    let extvdoCnt = document.getElementsByClassName('extvdo-total-played')[0].innerText;
    
    // set val of input field newValue
    //localStorage.setItem('extvdoPrependTimer', extvdo_prepend_timer);
    // input 
    var extvdo_prepend_timer = document.getElementById('extvdo-prepend-timer');
    
    var extvdoAppendTimer    = localStorage.getItem('extvdoPrependTimer');
    //console.log(extvdoAppendTimer);
    // add newValue + input 
    var a = parseInt(extvdoAppendTimer);
    var b = parseInt(extvdoCnt);
    var valextvdo = a+b;
    // remove oldValue 
    localStorage.setItem('extvdoPrependTimer', valextvdo );
    extvdo_prepend_timer.setAttribute('value', valextvdo );
    // update newValue
    //extvdo_prepend_timer.value = valextvdo;

    console.log(extvdoCnt + ' + ' + extvdoAppendTimer + ' = ' + valextvdo);
  });
}); 
